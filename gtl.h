#ifndef GTL_H
#define GTL_H

#include <vector>
#include <cstdio>

namespace MA{
/*
 *param: matrice de adiacenta, nr vf, nr muchii
 *citeste:
 *nr de vf, nr de muchii, muchii
 *creeaza:
 *matrice de adiacenta
 */
void readGM(int a[100][100], int &n, int &m){
	scanf("%d", &n); //nr de varfuri
	scanf("%d", &m); //nr de muchii
	//normalizeaza matrice
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j)
			a[i][j] = 0;
	int x, y;
	//citeste muchia
	for(int i = 1; i <= m; ++i){
		scanf("%d%d", &x, &y); //citeste nodurile care form muchia
		a[x][y] = a[y][x] = 1;
	}
}

/*
 *param: matrice de adiacenta, nr vf, nr muchii
 *afiseaza: matrice de adiacenta 
 */
void getM(int a[100][100], int &n, int &m){
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			if(a[i][j] == 1)
				printf("%d - %d\n", i, j); 
}

/*
 * param: matrice de adiacenta, nr vf, vf de verificat
 * ret: gradul vf de verificat
 */
int grad(int a[100][100], int n, int x){
	int gradX = 0;
	for(int i = 1; i <= n; ++i)
		if(a[x][i] == 1)
			++gradX;
	return gradX;
}
};

/*
 * structura simpla, pt a defini o muchie a unui graf
 */
struct muchie {
	int x, y;
};

/*
 * params: vector<muchie>, nr vf, nr muchii
 * citeste: nr vf, nr muchii, muchii
 * creeaza: vector dinamic de muchii
 */
void readGM(std::vector<muchie>& graf, int&n, int& m){
	scanf("%d", &n);
	scanf("%d", &m);
	for(int i = 0; i < m; ++i){
		int x, y;
		scanf("%d%d", &x, &y);
		muchie m;
		m.x = x, m.y = y;
		graf.push_back(m);
	}
}

/*
 * params: vector<muchie>
 * afiseaza: toate muchiile
 */
void getM(std::vector<muchie> graf){
	for(int i = 0; i < graf.size(); ++i)
		printf("%d - %d\n", graf[i].x, graf[i].y);
}

/*
 * params: vector<muchie>, vf
 * ret: grad vf
 */
int grad(std::vector<muchie> graf, int x){
	int gradX = 0;
	for(int i = 0; i < graf.size(); ++i)
		if(x == graf[i].x || x == graf[i].y)
			++gradX;
	return gradX;
}

/*
 * params: nr, vector<int>
 * ret: true daca nr e in vector, altfel false
 */
bool eIn(int x, std::vector<int> v){
	for(int i = 0; i < v.size(); ++i)
		if(x == v[i])
			return true;
	return false;
}

/*
 * params: nr vf, nr muchii
 * ret: true daca e un graf complet, altfel false
 */
bool eComplet(int vf, int m){
	return ((vf * (vf - 1)) / 2 == m);
}

/*
 * params: vector<muchie>, vector<int> mult 1, vector<int> mult 2, nr vf
 * ret: true daca e bip, false altfel
 */
bool eBip(std::vector<muchie> graf, std::vector<int> part1,\
				std::vector<int> part2, int n){
	for(int i = 0; i < graf.size(); ++i){
		if(!(eIn(graf[i].x, part1) && eIn(graf[i].y, part2)\
			|| eIn(graf[i].x, part2) && eIn(graf[i].y, part1)))
			return false;
	}
	if(part1.size() + part2.size() == n)
		return true;
	else return false;	
}

/*
 * params: vector<muchie>, vector<int> mult 1, vector<int> mult 2, nr vf
 * ret: true daca e bipc, false altfel
 */
bool eBipc(std::vector<muchie> graf, std::vector<int> part1,\
			std::vector<int> part2, int n){
		if(eBip(graf, part1, part2, n) && ((part1.size() * part2.size()\
				== graf.size())))
			return true;
		else return false;
}

/*
 * params: vector<muchie>, nr vf
 * ret: true daca e regulat, false altfel
 */
bool eReg(std::vector<muchie> graf, int n){
	int gradGen = grad(graf, 1);

	for(int i = 2; i <= n; ++i)
		if(gradGen != grad(graf, i))
			return false;		
	return true;
}
#endif
