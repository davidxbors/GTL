# G.T.L. - GRAPH TOY LIBRARY

O librărie simplă, care îndeplinește funcții de bază ale grafurilor.

##### Versiune actuală: 0.2

## TODO:

* parcurgeri de arbori
* arbore binar

## Docs

### readGM(std::vector<muchie>& graf, int&n, int& m) 

params: vector<muchie>, nr vf, nr muchii

citeste: nr vf, nr muchii, muchii

creeaza: vector dinamic de muchii

### void getM(std::vector<muchie> graf)

params: vector<muchie>

afiseaza: toate muchiile

### int grad(std::vector<muchie> graf, int x)

params: vector<muchie>, vf

ret: grad vf

### bool eIn(int x, std::vector<int> v)

params: nr, vector<int>

ret: true daca nr e in vector, altfel false

### bool eComplet(int vf, int m)

params: nr vf, nr muchii

ret: true daca e un graf complet, altfel false

### bool eBip(std::vector<muchie> graf, std::vector<int> part1, std::vector<int> part2, int n)

params: vector<muchie>, vector<int> mult 1, vector<int> mult 2, nr vf

ret: true daca e bip, false altfel

### bool eBipc(std::vector<muchie> graf, std::vector<int> part1, std::vector<int> part2, int n)

params: vector<muchie>, vector<int> mult 1, vector<int> mult 2, nr vf

ret: true daca e bipc, false altfel

### bool eReg(std::vector<muchie> graf, int n)

params: vector<muchie>, nr vf

ret: true daca e regulat, false altfel

## Teorie Grafuri:

[Adiacenta – Incidenta – Grad](https://tutoriale-pe.net/adiacenta-incidenta-grad/)

[Reprezentare](https://tutoriale-pe.net/reprezentare/)

[Graf partial si Subgraf](https://tutoriale-pe.net/graf-partial-si-subgraf/)

[Tipuri de grafuri neorientate](https://tutoriale-pe.net/tipuri-de-grafuri-neorientate/)

[Lant. Ciclu. Arbore](https://tutoriale-pe.net/lant-ciclu-arbore/)
